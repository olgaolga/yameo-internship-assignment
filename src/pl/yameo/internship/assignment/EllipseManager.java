/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.yameo.internship.assignment;

import java.util.List;

/**
 *
 * @author olgaz
 */
public class EllipseManager extends ShapeManager{

    

	public EllipseManager(GeometryApp ga) {
		super(ga);
	}

	@Override
	public String getName() {
		return Ellipse.name();
	}

	@Override
	public Shape setValues(Shape shape) {
		System.out.println("Please provide two semi-axis lengths (major, minor):");
		if (shape == null){
			shape = new Ellipse(ga.readDouble(),ga.readDouble());
		} else{
			((Ellipse) shape).setSemiMajorAxis(ga.readDouble());
			((Ellipse) shape).setSemiMinorAxis(ga.readDouble());
		}
		return shape;
	}
    
}
