package pl.yameo.internship.assignment;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

//added shapeManagerList to avoid if-else while creating and modifying shapes
//now to add new shape you should: 	-create new class which implements interface Shape,
//									-define shapeManager for this shape
//									-register shapeManager in list shapeManagerList in GeometryApp
//I added method readIntegerRange which reads Integer from given range
//I changed methods createNewShape() and modifyShape();

//I added tests for Circle and Ring, and made some little changes in existing tests for Square and Triangle

public class GeometryApp {
	private Scanner scanner;
	private List<ShapeManager> shapeManagerList = new ArrayList<>();
	private List<Shape> shapes = new ArrayList<>();

	public GeometryApp(Scanner scanner) {
		this.scanner = scanner;
		shapeManagerList.add(new CircleManager(this));
		shapeManagerList.add(new EllipseManager(this));
		shapeManagerList.add(new TriangleManager(this));
		shapeManagerList.add(new RectangleManager(this));
		shapeManagerList.add(new SquareManager(this));
		shapeManagerList.add(new ParallelogramManager(this));
		shapeManagerList.add(new RingManager(this));
	}

	public void start() {
		boolean run = true;
		while (run) {
			run = run();
		}
	}

	private boolean run() {
		System.out.println("Choose action:");
		System.out.println("1) Create new shape");
		System.out.println("2) List existing shapes");
		System.out.println("3) Modify one of the shapes from the list");
		System.out.println("0) Exit");

		int option = readIntegerRange(3);
		if (option == 0) {
			return false;
		} else if (option == 1) {
			Shape newShape = createNewShape();
			if (newShape != null) {
				shapes.add(newShape);
			}
		} else if (option == 2) {
			listShapes();
		} else if (option == 3) {
			modifyShape();
		}

		return true;
	}

	private Shape createNewShape() {		
		System.out.println("Choose shape to create:");
		for (int i=0;i<shapeManagerList.size();i++){
			System.out.println(Integer.toString(i+1)+") "+shapeManagerList.get(i).getName());
		}
		System.out.println("0) Back");
		int option = readIntegerRange(shapeManagerList.size());//readInteger changed to readIntegerRange
		if (option == 0){
			return null;
		} 
		ShapeManager sm = shapeManagerList.get(option-1); // if-else changed to shapeManagerList
		Shape s = sm.setValues(null);// setValues called with null to create new shape
		if(!sm.check(s)){ // if shape is not correct null is returned 
			System.out.println("Shape " + s.getName() + " is not correct and not added to list.");
			return null;
		}
		return s;	
	}

	private void listShapes() {
		Shape shape;
		System.out.println("====== LIST OF SHAPES ======");
		for(int i=0;i<shapes.size();i++){
			shape = shapes.get(i);
			System.out.print(i+1 + ") ");
			System.out.print(shape.getName() + " with dimensions: ");
			System.out.print(shape.listDimensions() + "; ");
			System.out.print("Area: " + shape.calculateArea() + "; ");
			System.out.println("Perimeter: " + shape.calculatePerimeter());
		}
		System.out.println("============================");
	}

	private void modifyShape() {
		listShapes();
		System.out.println("Please choose the index of the shape you want to modify (1-" + shapes.size() + "): ");
		int index = readIntegerRange(shapes.size());//readInteger changed to readIntegerRange
		Shape activeShape = shapes.get(index - 1);
		List<Double> oldDimensions = activeShape.listDimensions();
		Double oldPerimeter = activeShape.calculatePerimeter();
		Double oldArea = activeShape.calculateArea();

		System.out.print(activeShape.getName() + " with dimensions: ");
		System.out.print(oldDimensions + "; ");
		System.out.print("Area: " + oldArea + "; ");
		System.out.println("Perimeter: " + oldPerimeter);

		ShapeManager sm = getShapeManager(activeShape.getName()); // if-else changed to shapeManagerList
		if (sm!=null){
			sm.setValues(activeShape);//modify values for activeShape
			if(!sm.check(activeShape)){ //check if shape is correct, if not - old values are restored
				System.out.println("Shape " + activeShape.getName() + " is not correct. Old values are restored.");
			}
		}
		
		System.out.println("Old shape: ");
		System.out.print(activeShape.getName() + " with dimensions: ");
		System.out.print(oldDimensions + "; ");
		System.out.print("Area: " + oldArea + "; ");
		System.out.println("Perimeter: " + oldPerimeter);
		System.out.println("============================");
		System.out.println("New shape: ");
		System.out.print(activeShape.getName() + " with dimensions: ");
		System.out.print(activeShape.listDimensions() + "; ");
		System.out.print("Area: " + activeShape.calculateArea() + "; ");
		System.out.println("Perimeter: " + activeShape.calculatePerimeter());
		System.out.println("============================");

	}
	
	public Integer readIntegerRange(int range){
		boolean stop = false;
		Integer value = null;
		while(!stop){
			value = readInteger();
			stop = 0<=value && value<=range;
			if(!stop){
				System.out.println("Please type integer from 0 to " + range);
			}
		}
		return value;
	}
	
	private Integer readInteger() {
		Integer value = null;
		while (value == null) {
			if (scanner.hasNextInt()) {
				value = scanner.nextInt();
			} else {
				scanner.next();
			}
		}
		return value;
	}

	public Double readDouble() {
		Double value = null;
		while (value == null) {
			if (scanner.hasNextDouble()) {
				value = scanner.nextDouble();
			} else {
				scanner.next();
			}
		}

		return value;
	}
	
	private ShapeManager getShapeManager(String s){
		for (int i=0;i<shapeManagerList.size();i++){
			if (shapeManagerList.get(i).getName().equals(s)){
				return shapeManagerList.get(i);
			}
		}
		return null;
	}
}
