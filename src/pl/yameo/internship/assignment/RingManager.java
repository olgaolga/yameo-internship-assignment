package pl.yameo.internship.assignment;

public class RingManager extends ShapeManager {

	public RingManager(GeometryApp ga) {
		super(ga);
	}

	@Override
	public String getName() {
		return Ring.name();
	}
	
	@Override
	public Shape setValues(Shape shape) {
		System.out.println("Please provide the large radius and small radius for the ring:");
		if (shape == null){
			shape = new Ring(ga.readDouble(),ga.readDouble());
		} else{
			((Ring) shape).setLargeRadius(ga.readDouble());
			((Ring) shape).setSmallRadius(ga.readDouble());
		}
		return shape;
	}

}
