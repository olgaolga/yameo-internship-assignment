/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.yameo.internship.assignment;

import java.util.List;

/**
 *
 * @author olgaz
 */

//abstract class ShapeManager is created to avoid all if-else while creating and modifying shapes in GeometryApp
public abstract class ShapeManager {
    
	GeometryApp ga;
	public List<Double> oldDimensions; //old values for modify 
	public boolean modify; //true - modify, false - new shape
	
	//for access to scanner
	public ShapeManager(GeometryApp ga) {
		this.ga = ga;
	}
	
	//In every class that extends ShapeManager calling method setValues with null shape, new shape is created.
	//If that method is called with an existing shape, the new values are set.
    public Shape setValues(Shape shape){
    	modify = shape != null;
    	if (modify){
    		oldDimensions = shape.listDimensions(); //remember old dimensions
    	}
    	return shape;
    }
    
    //In every shape I added a static method name() which returns the name of the shape.
    //Method name() is called in the body of method getName() with the proper shape.
    public abstract String getName();
    
    //For every shape method check() returns true.
    //In TriangleManager it checks triangle and returns false if triangle is not correct.
    public boolean check(Shape shape){
    	return true;
    }
}
