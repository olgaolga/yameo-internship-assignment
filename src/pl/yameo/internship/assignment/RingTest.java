package pl.yameo.internship.assignment;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class RingTest {

	private static final double DELTA = 0.001;
	private static final double INITIAL_LARGE_RADIUS = 2.0;
	private static final double INITIAL_SMALL_RADIUS = 1.0;
	private static final double NEW_LARGE_RADIUS = 3.0;
	private static final double NEW_SMALL_RADIUS = 2.0;
	private static final double INITIAL_AREA = Math.PI*3;
	private static final double INITIAL_PERIMETER = Math.PI*6;
	private static final String SHAPE_NAME = "Ring";

	@Test
	public void when_Ring_created_then_it_has_proper_name() {
		Ring ring = new Ring(INITIAL_LARGE_RADIUS,INITIAL_SMALL_RADIUS);
		Assert.assertEquals(SHAPE_NAME, ring.getName());
	}
	
	@Test
	public void when_radius_set_then_dimension_is_changed() {
		Ring ring = new Ring(INITIAL_LARGE_RADIUS,INITIAL_SMALL_RADIUS);
		ring.setLargeRadius(NEW_LARGE_RADIUS);
		ring.setSmallRadius(NEW_SMALL_RADIUS);
		Assert.assertEquals(NEW_LARGE_RADIUS, ring.listDimensions().get(0), DELTA);
		Assert.assertEquals(NEW_SMALL_RADIUS, ring.listDimensions().get(1), DELTA);
	}
	
	@Test
	public void correct_ring_area(){
		Ring ring = new Ring(INITIAL_LARGE_RADIUS,INITIAL_SMALL_RADIUS);
		Assert.assertEquals(INITIAL_AREA, ring.calculateArea(), DELTA);
	}
	@Test
	public void correct_perimeter(){
		Ring ring = new Ring(INITIAL_LARGE_RADIUS,INITIAL_SMALL_RADIUS);
		Assert.assertEquals(INITIAL_PERIMETER, ring.calculatePerimeter(), DELTA);
	}
}
