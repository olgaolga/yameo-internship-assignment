/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.yameo.internship.assignment;

import java.util.List;

/**
 *
 * @author olgaz
 */
public class TriangleManager extends ShapeManager{

	public TriangleManager(GeometryApp ga) {
		super(ga);
	}
	
	//returns name of the Triangle with static method name()
	@Override
	public String getName() {
		return Triangle.name();
	}
	
	@Override
	public Shape setValues(Shape shape) {
		super.setValues(shape); //new or modify and remember old values
		System.out.println("Please provide three edge lengths:");
		
		if (shape == null){//create and init new triagle
			shape = new Triangle(ga.readDouble(),ga.readDouble(),ga.readDouble());
		} else{
			((Triangle) shape).setEdgeA(ga.readDouble()); //set new dimensions
			((Triangle) shape).setEdgeB(ga.readDouble());
			((Triangle) shape).setEdgeC(ga.readDouble());
		}
		return shape;
	}

	@Override
	public boolean check(Shape shape){
		Triangle triangle = (Triangle) shape;
		double edgeA = triangle.listDimensions().get(0);
		double edgeB = triangle.listDimensions().get(1);
		double edgeC = triangle.listDimensions().get(2);
		
		//checking the triangle inequality
		//if triangle is correct method returns true
		//if triangle is not correct for new shape check() does nothing, for existing shape it sets old dimensions.
		if (edgeA < edgeB +edgeC && edgeB < edgeC+ edgeA && edgeC < edgeA + edgeB){
			return true;
		} else{
			if(modify){
				triangle.setEdgeA(oldDimensions.get(0));
				triangle.setEdgeB(oldDimensions.get(1));
				triangle.setEdgeC(oldDimensions.get(2));
			}
			return false;
		}
			
	}


    
}
