package pl.yameo.internship.assignment;

import java.util.Arrays;
import java.util.List;

//added new shape Parallelogram which implements interface Shape 
public class Parallelogram implements Shape{
	
	private Double height = 0.0;
	private Double edgeA = 0.0;
	private Double edgeB = 0.0;
	
	public Parallelogram(Double height, Double edgeA, Double edgeB){
		this.height = height;
		this.edgeA = edgeA;
		this.edgeB = edgeB;
	}
	
	public static String name(){
		return "Parallelogram";
	}
	
	@Override
	public String getName() {
		return name();
	}

	@Override
	public List<Double> listDimensions() {
		return Arrays.asList(height, edgeA, edgeB);
	}

	@Override
	public Double calculateArea() {
		return edgeB * height;
	}

	@Override
	public Double calculatePerimeter() {
		return 2 * (edgeA + edgeB);
	}
	
	public void setHeight(Double height) {
		this.height = height;
	}

	public void setEdgeA(Double edgeA) {
		this.edgeA = edgeA;
	}
	
	public void setEdgeB(Double edgeB){
		this.edgeB = edgeB;
	}
}
