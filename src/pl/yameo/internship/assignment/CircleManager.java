/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.yameo.internship.assignment;

import java.util.List;

/**
 *
 * @author olgaz
 */
public class CircleManager extends ShapeManager{

	public CircleManager(GeometryApp ga) {
		super(ga);
	}
	
	@Override
	public String getName(){
		return Circle.name();
	}
	
	@Override
	public Shape setValues(Shape shape) {
		System.out.println("Please provide the radius for the circle:");
		if (shape == null){
			shape = new Circle(ga.readDouble());
		} else{
			((Circle) shape).setRadius(ga.readDouble());
		}
		return shape;
	}


}
