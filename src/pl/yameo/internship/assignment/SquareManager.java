/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.yameo.internship.assignment;

import java.util.List;

/**
 *
 * @author olgaz
 */
public class SquareManager extends ShapeManager{

	public SquareManager(GeometryApp ga) {
		super(ga);
	}
	
	@Override
	public String getName() {
		return Square.name();
	}

	@Override
	public Shape setValues(Shape shape) {
		System.out.println("Please provide the edge length:");
		if (shape == null){
			shape = new Square(ga.readDouble());
		} else{
			((Square) shape).setDimension(ga.readDouble());
		}
		return shape;
	}



    
}
