package pl.yameo.internship.assignment;

import java.util.Arrays;
import java.util.List;

public class Ring implements Shape {

	private Circle largeCircle;
	private Circle smallCircle;
	
	// Ring:
	// Area: abs(largeCircle area - smallCircle area)
	// Perimeter: largeCircle perimeter + smallCircle perimeter
	
	public Ring(Double largeRadius, Double smallRadius) {
		largeCircle = new Circle(largeRadius);
		smallCircle = new Circle(smallRadius);
	}

	public static String name(){
		return "Ring";
	}

	@Override
	public String getName() {
		return name();
	}

	@Override
	public List<Double> listDimensions() {
		return Arrays.asList(largeCircle.listDimensions().get(0), smallCircle.listDimensions().get(0));
	}

	@Override
	public Double calculateArea() {
		return Math.abs(largeCircle.calculateArea()-smallCircle.calculateArea());
	}

	@Override
	public Double calculatePerimeter() {
		return largeCircle.calculatePerimeter() + smallCircle.calculatePerimeter();
	}
	
    public void setLargeRadius(Double radius) {
    	largeCircle.setRadius(radius);
    }
	
    public void setSmallRadius(Double radius) {
    	smallCircle.setRadius(radius);
    }
}
