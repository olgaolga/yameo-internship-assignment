/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.yameo.internship.assignment;

import java.util.List;

/**
 *
 * @author olgaz
 */
public class RectangleManager extends ShapeManager{

	public RectangleManager(GeometryApp ga) {
		super(ga);
	}

	@Override
	public String getName() {
		return Rectangle.name();
	}
	
	@Override
	public Shape setValues(Shape shape) {
		System.out.println("Please provide two edge lengths (height, width):");
		if (shape == null){
			shape = new Rectangle(ga.readDouble(),ga.readDouble());
		} else{
			((Rectangle) shape).setHeight(ga.readDouble());
			((Rectangle) shape).setWidth(ga.readDouble());
		}
		return shape;
	}




    
}
