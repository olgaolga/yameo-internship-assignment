package pl.yameo.internship.assignment;

public class Square extends Rectangle {
	
	private Double height = 0.0;
	private Double width = 0.0;
	
	public Square(Double dimension) {
		super(dimension, dimension);
	}

	public static String name(){
		return "Square";
	}
	
	@Override
	public String getName() {
		return name();
	}

	@Override
	public void setHeight(Double height) {
		setDimension(height);
	}

	@Override
	public void setWidth(Double width) {
		setDimension(width);
	}

	public void setDimension(Double dimension) {
		super.setHeight(dimension);
		super.setWidth(dimension);
	}
}
