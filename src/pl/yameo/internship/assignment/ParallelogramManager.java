package pl.yameo.internship.assignment;

public class ParallelogramManager extends ShapeManager{

	public ParallelogramManager(GeometryApp ga) {
		super(ga);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getName() {
		return Parallelogram.name();
	}
	
	@Override
	public Shape setValues(Shape shape) {
		System.out.println("Please provide height and two edge lengths (height, edgeA, edgeB):");
		if (shape == null){
			shape = new Parallelogram(ga.readDouble(),ga.readDouble(),ga.readDouble());
		} else{
			((Parallelogram) shape).setHeight(ga.readDouble());
			((Parallelogram) shape).setEdgeA(ga.readDouble());
			((Parallelogram) shape).setEdgeB(ga.readDouble());
		}
		return shape;
	}



}
