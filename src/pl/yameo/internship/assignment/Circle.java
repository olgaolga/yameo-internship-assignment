package pl.yameo.internship.assignment;

import java.util.Arrays;
import java.util.List;


public class Circle implements Shape {
	
    private Double radius = 0.0;
    
    public static String name(){
    	return "Circle";
    }
    
	@Override
	public String getName() {
		return Circle.name();
	}
    
    public Circle(Double radius) {
        this.radius = radius;
	}

    public void setRadius(Double radius) {
    	this.radius = radius;
    }

    @Override
    public List<Double> listDimensions() {
    	return Arrays.asList(radius);
    }

    @Override
    public Double calculateArea() {
    	return Math.PI *radius*radius;
    }

    @Override
    public Double calculatePerimeter() {
    	return Math.PI *radius*2;
    }
    
}
