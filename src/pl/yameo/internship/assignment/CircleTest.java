package pl.yameo.internship.assignment;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class CircleTest {

	private static final double DELTA = 0.001;
	private static final double INITIAL_RADIUS = 1.0;
	private static final double INITIAL_AREA = 3.141;
	private static final double INITIAL_PERIMETER = Math.PI*2;
	private static final double NEW_RADIUS = 3.0;
	private static final String SHAPE_NAME = "Circle";
	
	@Test
	public void when_circle_created_then_it_has_proper_name() {
		Circle circle = new Circle(INITIAL_RADIUS);
		Assert.assertEquals(SHAPE_NAME, circle.getName());
	}

	@Test
	public void when_radius_set_then_dimension_is_changed() {
		Circle circle = new Circle(INITIAL_RADIUS);
		circle.setRadius(NEW_RADIUS);
		Assert.assertEquals(NEW_RADIUS, circle.listDimensions().get(0), DELTA);
	}
	
	@Test
	public void correct_circle_area(){
		Circle circle = new Circle(INITIAL_RADIUS);
		Assert.assertEquals(INITIAL_AREA, circle.calculateArea(),DELTA);
	}
	
	@Test
	public void correct_perimeter(){
		Circle circle = new Circle(INITIAL_RADIUS);
		Assert.assertEquals(INITIAL_PERIMETER, circle.calculatePerimeter(),DELTA);
	}
}
